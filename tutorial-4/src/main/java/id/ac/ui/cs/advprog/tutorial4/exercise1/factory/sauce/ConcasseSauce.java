
package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ConcasseSauce implements Sauce {
    public String toString() {
        return "Concasse Sauce";
    }
}
