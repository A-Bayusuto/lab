package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class EmmentalCheese implements Cheese {

    public String toString() {
        return "Shredded Emmental";
    }
}