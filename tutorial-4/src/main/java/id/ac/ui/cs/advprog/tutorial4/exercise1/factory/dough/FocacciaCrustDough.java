package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FocacciaCrustDough implements Dough {
    public String toString() {
        return "Focaccia style crust";
    }
}
